using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CatController : MonoBehaviour
{
    private bool puedeSaltar = false;
    private SpriteRenderer sr;
    private Animator _animator;
    private Rigidbody2D rb2d;
    public Text textScoreGold;
    public Text textScoreSilver;
    public Text textScoreBronze;
    public Text textFullScore;
    public int scoreGold = 0;
    public int scoreSilver = 0;
    public int scoreBronze = 0;
    public int fullScore = 0;
    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        textScoreGold.text = "Gold: " + scoreGold;
        textScoreSilver.text = "Silver: " + scoreSilver;
        textScoreBronze.text = "Bronze: " + scoreBronze;
        textFullScore.text = "Total: " + fullScore;

        if (Input.GetKey(KeyCode.RightArrow))
        {
            sr.flipX= false;
            setRunAnimation();
            rb2d.velocity = new Vector2(10,rb2d.velocity.y);
        }
        else
        {
            setIdleAnimation();
            rb2d.velocity = new Vector2(0,rb2d.velocity.y);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            sr.flipX = true;
            setRunAnimation();
            rb2d.velocity = new Vector2(-10,rb2d.velocity.y);
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            setSlideAnimation();
        }

        if (Input.GetKeyDown(KeyCode.Space) && puedeSaltar)
        {
            float upSpeed = 45;
            setJumpAnimation();
            rb2d.velocity = Vector2.up * upSpeed;
            puedeSaltar = false;
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        puedeSaltar = true;
        if(other.collider.tag == "GoldCoin")
        {
            Destroy(other.gameObject);
            RecojoGold();
            IncrementarPuntajeEn10();
        }
        if(other.collider.tag == "SilverCoin")
        {
            Destroy(other.gameObject);
            RecojoSilver();
            IncrementarPuntajeEn5();
        }
        if(other.collider.tag == "BronzeCoin")
        {
            Destroy(other.gameObject);
            RecojoBronze();
            IncrementarPuntajeEn1();
        }
    }

    private void setRunAnimation(){
       _animator.SetInteger("State",1);
    }
    
    private void setIdleAnimation(){
        _animator.SetInteger("State",0);
    }

    private void setSlideAnimation(){
        _animator.SetInteger("State",3);
    }
    
    private void setJumpAnimation(){
        _animator.SetInteger("State",2);
    }

    public void IncrementarPuntajeEn10()
    {
        fullScore += 10;
    }

    public void IncrementarPuntajeEn5()
    {
        fullScore += 5;
    }

    public void IncrementarPuntajeEn1()
    {
        fullScore += 1;
    }

    public void RecojoGold()
    {
        scoreGold += 1;
    }

    public void RecojoSilver()
    {
        scoreSilver += 1;
    }

    public void RecojoBronze()
    {
        scoreBronze += 1;
    }
}
